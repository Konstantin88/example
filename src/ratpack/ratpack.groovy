import static ratpack.groovy.Groovy.ratpack
import org.example.QuoteService

ratpack {
  handlers {
    get {
      render new QuoteService().any()
    }
  }
}
